<?php

/**
 * @file
 * Add headerid to all content header tags.
 */

/**
 * Filter modes
 */
define('HEADERID_REWRITE_MODE_NONE', 'none');
define('HEADERID_REWRITE_MODE_FIX', 'fix');
define('HEADERID_REWRITE_MODE_REWRITE', 'rewrite');

/**
 * Implements hook_filter_info().
 */
function headerid_filter_info() {
  $filters['headerid'] = array(
    'title' => t('Header ID'),
    'description' => t("Add 'id' attribute to all header tags in the content"),
    'default settings' => array(
      'rewrite_mode' => HEADERID_REWRITE_MODE_NONE,
      'default_prefix' => 'h',
      'headers' => array('h1', 'h2', 'h3'),
    ),
    'settings callback' => '_headerid_settings_callback',
    'process callback' => '_headerid_process_callback',
    'weight' => 99,
  );
  return $filters;
}

/**
 *  Header ID filter processor callback.
 *
 * @see headerid_filter_info()
 */
function _headerid_process_callback($text, $filter, $format, $langcode, $cache, $cache_id) {
  module_load_include('inc', 'headerid');
  $generator = new HeaderIdGenerator(new HeaderIdRegistry(), $filter->settings['default_prefix']);

  try {
    $html_dom = filter_dom_load($text);
    // Construct xpath selector like this: "//h1|//h2|//h3|//h4|//h5|//h6"
    $headers_selector = '//' . implode('|//', $filter->settings['headers']);
    $dom_xpath = new DOMXPath($html_dom);
    $headers = $dom_xpath->query($headers_selector);

    /** @var $header DOMElement */
    foreach ($headers as $header) {
      $header_id = trim($header->getAttribute('id'));
      // Header ID exist and rewrite all mode is off.
      if ($header_id && $filter->settings['rewrite_mode'] != HEADERID_REWRITE_MODE_REWRITE) {
        // Check if the ID is unique and add to registry.
        if ($generator->exist($header_id)) {
          if ($filter->settings['rewrite_mode'] == HEADERID_REWRITE_MODE_FIX) {
            $id_prefix = trim($header_id, '0123456789');
            $new_header_id = $generator->create($id_prefix);
            $header->setAttribute('id', $new_header_id);
          }
        }
        else {
          $generator->add($header_id);
        }
      }
      // Header ID doesn't exist or mode is set to rewrite all. We need to create one.
      else {
        $new_header_id = $generator->create();
        $header->setAttribute('id', $new_header_id);
      }
    }

    $text = filter_dom_serialize($html_dom);
  }
  catch (Exception $e) {
    watchdog('headerid', 'Filter error: @error', array('@error' => $e->getMessage()));
  }

  return $text;
}

/**
 * Header ID filter settings callback.
 *
 * @see headerid_filter_info()
 */
function _headerid_settings_callback($form, &$form_state, $filter, $format, $defaults, $filters) {
  $filter->settings += $defaults;

  $filter_form['rewrite_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Existing header IDs'),
    '#options' => array(
      HEADERID_REWRITE_MODE_NONE => t('Do not touch'),
      HEADERID_REWRITE_MODE_FIX => t('Fix conflicts'),
      HEADERID_REWRITE_MODE_REWRITE => t('Rewrite all'),
    ),
    '#description' => t('Select what to do with the existing ids.'),
    '#default_value' => $filter->settings['rewrite_mode'],
  );
  $filter_form['default_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix for automatically generated ID'),
    '#default_value' => $filter->settings['default_prefix'],
    '#required' => TRUE,
  );
  $filter_form['headers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Header tags'),
    '#description' => t('Selected header tags which will be processed by this filter.'),
    '#options' => array(
      'h1' => 'h1',
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
      'h5' => 'h5',
      'h6' => 'h6',
    ),
    '#required' => TRUE,
    '#default_value' => $filter->settings['headers'],
  );

  return $filter_form;
}
